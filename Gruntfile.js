var matchdep = require('matchdep');

module.exports = function(grunt) {
  matchdep.filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.initConfig({
    sass: {
      dev: {
        files: { 'public/application.css': 'scss/namek-shoutbox.scss' }
      }
    },

    watch: {
      sass: {
        files: ['scss/**/*.scss'],
        tasks: ['sass']
      }
    },

    nodemon: {
      dev: {}
    },

    concurrent: {
      main: {
        tasks: ['nodemon', 'watch']
      },
      options: {
        logConcurrentOutput: true
      }
    },

    knexmigrate: function(cb) {
      cb(null, require('config'));
    }
  });

  grunt.registerTask('compile', ['sass']);
  grunt.registerTask('server', ['compile', 'concurrent']);
};