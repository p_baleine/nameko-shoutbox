
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('users', function(t) {
      t.string('salt').notNull().defaultTo('');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('users', function(t) {
      t.dropColumn('salt');
    })
  ]);
};
