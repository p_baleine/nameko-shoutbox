
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('users', function(t) {
      t.dateTime('created_at').notNull().defaultTo(new Date());
      t.dateTime('updated_at');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return knex.schema.table('users', function(t) {
    t.dropColumns('created_at', 'updated_at');
  });
};
