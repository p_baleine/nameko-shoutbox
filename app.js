var express = require('express'),
    Resource = require('express-resource'),
    path = require('path'),

    session = require('./routes/session'),

    app = module.exports = express();

app.set('views', 'views');
app.set('view engine', 'jade');

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
app.use(express.session());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

app.resource('sessions', session);

if (!module.parent) {
  app.listen(3000);
  console.log('nameko listening on 3000!');
}