var Promise = require('bluebird'),
    bcrypt = Promise.promisifyAll(require('bcrypt')),
    BaseModel = require('./base');

// User model
var User = module.exports = BaseModel.extend({

  tableName: 'users',

  hasTimestamps: true,

  initialize: function() {
    BaseModel.prototype.initialize.apply(this, arguments);
    // hash password on sabing.
    this.on('saving', this.hashPassword, this);
  },

  // swap password field with hashed one.
  hashPassword: function() {
    var _this = this;

    // first generate salt
    return bcrypt.genSaltAsync(10)
      .then(function(salt) {
        _this.set('salt', salt);
        // and hash password with generated salt
        return bcrypt.hashAsync(_this.get('password'), salt);
      })
      .then(function(hash) {
        // and set password with hashed one.
        return _this.set('password', hash);
      });
  }

}, {

  // Authenticate user with `email` and `password`.
  authenticate: function(email, password) {
    // find user by `email`
    return new this({ email: email }).fetch({ require: true })
      .then(function(user) {
        // hash the fetched user's password
        return [bcrypt.hashAsync(password, user.get('salt')), user];
      })
      .spread(function(hash, user) {
        // compare hashed password with stored password.
        if (hash === user.get('password')) { return user; }
        throw new Error('password or email is incorrect.');
      });
  }

});
