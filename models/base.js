var Bookshelf = require('bookshelf'),

    namekoBookShelf = Bookshelf.initialize(require('config').database);

Bookshelf.namekoBookShelf = namekoBookShelf;

var BaseModel = namekoBookShelf.Model.extend({
}, {

  findAll: function() {
    var coll = namekoBookShelf.Collection.forge({ model: this });
    coll.fetch.apply(coll, arguments);
  }

});

module.exports = BaseModel;


